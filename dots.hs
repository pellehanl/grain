{-# LANGUAGE OverloadedStrings #-}
import Graphics.Svg
import Graphics.Image hiding (map, on)
import System.Random
import Data.Text (pack)
import Data.Function
import Control.Monad
import System.Environment
d = (/) `on` fromIntegral

grey :: Pixel RGB Double -> Double
grey (PixelRGB r g b) = (r + g + b)/3

rndbool :: Double -> IO Bool
rndbool p = fmap (p <) (getStdRandom (randomR (0,1)))

pl :: Image VU RGB Double -> [(Int, Int)] -> [Pixel RGB Double]
pl img = map (defaultIndex (PixelRGB 1 1 1) img)

remain :: [(a, Bool)] -> [a]
remain l = map fst $ filter snd l

putCircle :: Double -> (Int, Int) -> Element -- r is in percent. deal with it
putCircle r (x,y) = circle_ [Cx_ <<- pack (show y), Cy_ <<- pack (show x), R_ <<- pack (show r ++ "%"), Fill_ <<- "black"] 
-- we assume a quadratic image.

ctr :: (Int, Int) -> (Int, Int) -> (Int, Int) -- transform svg canvas coords to img coords
ctr (ix,iy) (sx,sy)= (floor ((sx `d` 1000)* fromIntegral ix), floor ((sy `d` 1000)* fromIntegral iy))
ctrpl img l = pl img $ map (ctr (dims img)) l
svg :: Element -> Element
svg c = doctype <> with (svg11_ c) [Version_ <<- "1.1", Width_ <<- "1000", Height_ <<- "1000"]

rn n = replicateM n $ randomRIO (0,1000::Int)

sigmoid :: Fractional a => a -> a
sigmoid = undefined

main :: IO ()
main = do
    args <- getArgs
    img <- readImageRGB VU $ head args
    let n = read $ args!!1 :: Int
    let q = read $ args!!2 :: Double
    let r = read $ args!!3 :: Double
    rxl <- rn n
    ryl <- rn n
    let c = zip rxl ryl
    let px = zip c $ map grey(ctrpl img (zip rxl ryl))
    --let m = remain $ zip c px
    let a = foldl (<>) mempty $ fmap (\(x,l) -> putCircle ((1-l)*r) x) px
    print $ svg a

